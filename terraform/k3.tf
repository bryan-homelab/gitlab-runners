terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9"
    }
    pihole = {
      source = "ryanwholey/pihole"
    }
  }
}


provider "proxmox" {
  pm_tls_insecure = true
  pm_api_url      = var.pve_url
  pm_password     = var.pve_password
  pm_user         = "root@pam"
}

provider "pihole" {
  url = var.pihole_url

  api_token = var.pihole_api_token # PIHOLE_API_TOKEN
}


module "pve_vm" {
  source = "git@gitlab.com:bryan-homelab/homelab-terraform-modules.git//pve_vm_with_pihole"

  for_each = var.vms

  vm_name    = each.value.name
  vm_ip      = each.value.ip
  vm_gateway = "10.10.10.1"
  vm_domain  = "homelab.com"
  node       = each.value.node

  cpu    = each.value.cpu
  memory = each.value.memory
}
